#!/usr/bin/env qore
%enable-all-warnings
%new-style
%requires CsvUtil

CsvFileIterator csvFile("./var/temp/example-products.csv", (
	"encoding": "UTF8",
	"header_names": 1,
	"header_lines": 1,
	"verify_columns": 1,
	"fields": (
		"IMEI": "int",
		"PartNmbr": "int",
		"CustNmbr": "int",
		"Deldate": (
			"type": "date",
			"format": "DD/MM/YYYY"
		),
		"Shipper_CustNmbr": "int",
		"Orderref": "string",
		"CustomerName": "string",
		"Description": "string"
	)
));

#printf("Header: %y\n", csvFile.getHeaders());

map printf("%y\n", $1), csvFile;